// load("lodash.min.js");
// load("extensions.js");

print("hello"); // output: hello

var character = new Character();

character.name = "Teppo";
print(character.name); // output: teppo 

character.pahkina = "kookos";
print(character.pahkina); // output: kookos 

character.height = 172.4;
character.weight = 84.2;
character.bmi = 12; // bmi is getter only, setting has no effect 


print(character.bmi); // output: 58.004640371229705 
print(character.isFat); // output: true 

print(character.sayHello());

