# Scripter

Small test project to test Java 8 Nashorn JavaScript executor.


## Sources

- https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/prog_guide/javascript.html
- https://docs.oracle.com/javase/8/docs/jdk/api/nashorn/jdk/nashorn/api/scripting/JSObject.html#getMember-java.lang.String-
- http://stackoverflow.com/questions/20793089/secure-nashorn-js-execution
- http://winterbe.com/posts/2014/04/05/java8-nashorn-tutorial/