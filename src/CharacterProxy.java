import jdk.nashorn.api.scripting.AbstractJSObject;
import jdk.nashorn.api.scripting.JSObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by hki on 8.6.2016.
 */
public class CharacterProxy implements JSObject {

    private Character character;
    private HashMap<String, Object> extraMembers;

    public CharacterProxy(){
        character = new Character();
        extraMembers = new HashMap<>();

    }

    @Override
    public Object call(Object o, Object... objects) {
        return null;
    }

    @Override
    public Object newObject(Object... objects) {
        return new CharacterProxy();
    }

    @Override
    public Object eval(String s) {
        return null;
    }

    @Override
    public Object getMember(String s) {

        switch (s){
            case "name":
                return character.getName();
            case "height":
                return character.getHeight();

            case "weight":
                return character.getWeight();
            case "bmi":
                return character.getBMI();
            case "isFat":
                return character.isFat();
            case "sayHello":
                return this.helloCall();
            default:
                if(extraMembers.containsKey(s)){
                    return extraMembers.get(s);
                }else{
                    return null;
                }
        }
    }

    @Override
    public Object getSlot(int i) {
        return null;
    }

    @Override
    public boolean hasMember(String s) {

        switch (s){
            case "name":
            case "height":
            case "weight":
            case "bmi":
            case "isFat":
                return true;
            default:
                if(extraMembers.containsKey(s)){
                    return true;
                }else{
                    return false;
                }
        }

    }

    @Override
    public boolean hasSlot(int i) {
        return false;
    }

    @Override
    public void removeMember(String s) {
        if(extraMembers.containsKey(s)){
            extraMembers.remove(s);
        }
    }

    @Override
    public void setMember(String s, Object o) {
        switch (s){
            case "name":
                character.setName((String) o);
                break;
            case "height":
                character.setHeight((Double) o);
                break;
            case "weight":
                character.setWeight((Double) o);
                break;
            case "bmi":
            case "isFat":
                break;
            default:
                extraMembers.put(s,o);

        }
    }

    @Override
    public void setSlot(int i, Object o) {

    }

    @Override
    public Set<String> keySet() {
        return null;
    }

    @Override
    public Collection<Object> values() {
        return null;
    }

    @Override
    public boolean isInstance(Object o) {
        return false;
    }

    @Override
    public boolean isInstanceOf(Object o) {
        return false;
    }

    @Override
    public String getClassName() {
        return null;
    }

    @Override
    public boolean isFunction() {
        return true;
    }

    @Override
    public boolean isStrictFunction() {
        return false;
    }

    @Override
    public boolean isArray() {
        return false;
    }

    /**
     * @deprecated
     */
    @Override
    public double toNumber() {
        return 0;
    }


    private AbstractJSObject helloCall(){

        return new AbstractJSObject() {
            @Override
            public Object call(Object thiz, Object... args) {
                return character.toString();
            }
            // yes, I'm a function !
            @Override
            public boolean isFunction() {
                return true;
            }
        };

    }

}
