import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) {

        NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine(getEngineArguments());

        Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);

        //bindings.remove("load"); // load can be extremely unsafe
        bindings.put("Character", new CharacterFactoryProxy());

        try {
            String fileLocation = args[0];
            engine.eval(Main.getScriptFile(fileLocation));
        } catch (ScriptException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


    private static FileReader getScriptFile(String file) throws FileNotFoundException {
        return new FileReader(file);
    }

    private static String[] getEngineArguments(){
        return new String[] {
                "-strict",
                "--no-java",
                "--no-syntax-extensions"
        };
    }
}
