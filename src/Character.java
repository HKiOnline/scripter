/**
 * Created by hki on 8.6.2016.
 */
public class Character {

    private String name;
    private Double height; // in centimeters
    private Double weight; // in kilograms

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public double getBMI() {

        Double heightInMeters = this.getHeight() / 100;
        Double bmi = this.getWeight() / (heightInMeters * heightInMeters);
        return bmi;
    }

    public boolean isFat(){
        if (getBMI() >= 30 ){
            return true;
        }else {
            return false;
        }
    }

   @Override
   public String toString(){

       StringBuilder stringBuilder = new StringBuilder();

       stringBuilder.append("Hello! My name is " + this.getName() +".\n");
       stringBuilder.append("I'm " + this.getHeight() + " centimeters tall and I weigh " + this.getWeight() + " kilograms.\n");
       stringBuilder.append("Some people call me fat and that's " + this.isFat() + ".");

       return stringBuilder.toString();

   }
}
